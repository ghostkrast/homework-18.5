﻿#include <iostream>
#include <stack>

using namespace std;

int main()
{
	stack <int> steck;
	int i = 0;
	cout << "Enter 2 numbers:" << endl;
	while (i != 2)
	{
		int a;
		cin >> a;
		steck.push(a);
		i++;
	}
	cout << "The top element of steck:" << steck.top() << endl;
	cout << "Deleting the top element? 1 - yes, 2 - no" << endl;
	{
		int b;
		cin >> b;
		if (b == 1)
		{
			steck.pop();
			cout << "The new top element of steck:" << steck.top() << endl;
		}
		if (b != 1)
		{
			cout << "The element is not deleting" << endl;
		}
	}
}